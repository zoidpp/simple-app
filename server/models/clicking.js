module.exports = (sequelize, DataTypes) => {
  const clicking = sequelize.define('clicking', {
    amount: {
      type: DataTypes.INTEGER
    },
  });

  return clicking;
};
