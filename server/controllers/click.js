const Click = require('../models').clicking;

self = module.exports = {
  create(req, res) {
    return Click
      .create({
        amount: 1,
      })
      .then(click => res.status(201).send(click))
      .catch(error => res.status(400).send(error));
  },
  list(req, res) {
    return Click
      .findAll()
      .then(click => res.status(200).send(click))
      .catch(error => res.status(400).send(error));
  },
  retrive(req, res) {
    return Click
      .findAll({
        limit: 1
      })
      .then(click => {
        if (click.length == 0) {
          self.create(req, res)
        } else
            return res.status(200).send(click);
      })
      .catch(error => res.status(400).send(error));
  },
  updateIfClick(req, res) {
    return Click
      .findOrCreate({
        where:{
          id: 1
        }, defaults: {
          amount: 1
        }
      })
      .then(click => {
        return click
          .update({
            amount: click.amount+1
          })
          .then(() => res.status(200).send(click))
          .catch((error) => res.status(401).send(error));
      })
      .catch((error) => res.status(402).send(error));
  },
  addClick(req,res) {
    return Click
      .findById(1)
      .then(click => {
        if (!click) {
          return res.status(404).send({
            message: 'No one clicked !',
          });
      }
      return click
        .update({
          amount: click.amount + 1
        })
        .then(() => res.status(200).send(click))
        .catch((error) => res.status(401).send(error));
      })
      .catch((error) => res.status(402).send(error));
  }
};
