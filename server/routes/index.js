const clicksController = require('../controllers').clicks;

module.exports = (app) => {
  app.get('/api', (req, res) => res.status(200).send({
    message: 'Click api Online!',
  }));
 
  app.get('/api/clicking', clicksController.create);
  app.get('/api/clicks', clicksController.list);
  app.get('/api/first', clicksController.retrive);
  app.get('/api/click', clicksController.addClick);
  app.get('/api/update', clicksController.updateIfClick);
  app.get('/play', function(req, res){
    res.render('play', {
      click: { amount: 42 }
    });
  });
  
};
